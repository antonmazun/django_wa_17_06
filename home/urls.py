from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    path('article-detail/<int:id_article>' , views.detail_article),
    path('calc/' , views.calc),
    path('examples/' , views.examples),
    path('add-article/' , views.add_article),
]
