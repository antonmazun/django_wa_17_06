from django.db import models
import datetime

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=1000)
    price = models.FloatField(default=0)
    create_at = models.DateTimeField(auto_created=True , default=datetime.datetime.now())


    def __str__(self):
        return '{} {}$'.format(self.title, self.price)
