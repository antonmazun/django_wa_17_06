from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Article
from .calc import calc_obj
from .forms import ArticleForm


# Create your views here.

def home(request):
    ctx = {}
    all_articles = Article.objects.all()
    ctx['articles'] = all_articles
    ctx['active_tab'] = 'home'

    return render(request, 'home/home_page.html', ctx)


def detail_article(r, id_article):
    try:
        article = Article.objects.get(id=id_article)
    except Exception as e:
        return HttpResponse('Article not found!!!!')
    return render(r, 'home/detail_article.html', {
        'article': article
    })


def calc(request):
    return render(request, 'home/calc.html', {

    })


def examples(request):
    ctx = {}
    ctx['one'] = 1
    ctx['active_tab'] = 'examples'
    ctx['extra_header'] = [
        {
            'name_tab': 'Admin',
            'url': '/admin'
        },
        {
            'name_tab': 'Google',
            'url': 'https://www.google.com/'
        }
    ]
    user_text = request.GET.get('user_text')
    user_password = request.GET.get('user_password')
    search = request.GET.get('search')
    range = request.GET.get('range')
    if user_text and range:
        ctx['user_text_range'] = user_text * int(range)
    if request.method == 'POST':
        first = float(request.POST.get('first'))
        operation = request.POST.get('operation')
        second = float(request.POST.get('second'))
        if operation in calc_obj:
            ctx['result'] = calc_obj[operation](first, second)

    ctx['form'] = ArticleForm()
    return render(request, 'home/examples.html', ctx)


def add_article(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/examples/')
    else:
        return HttpResponse('Method GET not allowed!')


def page_not_found(request , exception):
    return render(request , '404.html' , {})