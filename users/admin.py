from django.contrib import admin
from .models import UserAuthor , Order
# Register your models here.


admin.site.register(UserAuthor)
admin.site.register(Order)