from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import UserAuthor, Book, Order


class SingUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=150, required=False, label='Имя')
    second_name = forms.CharField(max_length=150, required=False, label='Фамилия')
    email = forms.EmailField(max_length=200, required=False)

    def save(self, *args, **kwargs):
        data = self.cleaned_data
        django_user = User.objects.create_user(
            username=data['username'],
            password=data['password1'],
            first_name=data['first_name'],
            last_name=data['second_name'],
            email=data['email']
        )
        UserAuthor.objects.create(user=django_user)
        return django_user


class UserAuthorForm(forms.ModelForm):
    class Meta:
        model = UserAuthor
        exclude = 'user',


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = ('author',)

    def save(self, *args, **kwargs):
        current_user = kwargs.pop('author')
        if current_user:
            self.instance.author = current_user
            self.instance.save()


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ('books', 'total_price')

    def save(self, *args, **kwargs):
        print('custom save!!!!')
        new_order = Order(
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name'],
            middle_name=self.cleaned_data['middle_name'],
            email=self.cleaned_data['email'],
            phone_number=self.cleaned_data['phone_number'],
            total_price=kwargs.get('total_price', 0),
            msg=self.cleaned_data['msg']
        )
        new_order.save()
        for book in kwargs['books']:
            new_order.books.add(book.id)

