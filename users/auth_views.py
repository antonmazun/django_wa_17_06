from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import SingUpForm
from django.contrib.auth import authenticate, login as django_login, logout as django_logout

from django.contrib.auth import authenticate


# Create your views here.


def register(request):
    ctx = {}
    ctx['form'] = SingUpForm
    if request.method == 'POST':
        form = SingUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = user.username
            password = form.cleaned_data['password2']
            auth_user = authenticate(username=username,
                                     password=password)
            if auth_user:
                django_login(request, auth_user)
        else:
            ctx['form'] = form
    return render(request, 'users/auth/register.html', ctx)


def logout(request):
    django_logout(request)
    return redirect('/')


def login(request):
    ctx = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        auth_user = authenticate(username=username, password=password)
        if auth_user:
            django_login(request, auth_user)
            return redirect('/')
        else:
            ctx['error'] = 'User not found!'
    return render(request, 'users/auth/login.html', ctx)
