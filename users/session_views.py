from django.shortcuts import render, redirect
from .models import Book
from .forms import OrderForm


def get_books_from_bucket(req):
    books_in_sessions = req.session.get('books_bucket', [])
    return [Book.objects.get(id=id_) for id_ in books_in_sessions]


def add_to_session(request, book_id):
    books_in_sessions = request.session.get('books_bucket', [])
    if book_id not in books_in_sessions:
        books_in_sessions.append(book_id)
        request.session['books_bucket'] = books_in_sessions
        print(request.session['books_bucket'])
    return redirect('users:books')


def bucket(request):
    ctx = {}
    all_books = get_books_from_bucket(req=request)
    ctx['books'] = all_books
    ctx['sum'] = sum([book.price for book in all_books])
    return render(request, 'users/bucket.html', ctx)


def delete_from_bucket(request, pk):
    books_in_sessions = request.session.get('books_bucket', [])
    if pk in books_in_sessions:
        books_in_sessions.remove(pk)
        request.session['books_bucket'] = books_in_sessions
        return redirect('users:bucket')


def approve_order(request):
    ctx = {}
    all_books = get_books_from_bucket(req=request)
    total_price = sum([book.price for book in all_books])
    ctx['all_books'] = all_books
    ctx['form'] = OrderForm
    ctx['total_price'] = total_price

    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save(books=all_books, total_price=total_price)
            print('valid form')
        else:
            print('invalid form!')
        request.session['books_bucket'] = []
    return render(request, 'users/approve_order.html', ctx)
