from django.shortcuts import render, redirect
from .models import UserAuthor, Book, Order
from .forms import UserAuthorForm, BookForm


# Create your views here.


def cabinet(request):
    ctx = {}
    ctx['cabinet_tab'] = 'main'
    if request.user.is_authenticated:
        author = UserAuthor.objects.get(user_id=request.user.id)
        form = UserAuthorForm(instance=author)
        if request.method == 'POST':
            form = UserAuthorForm(instance=author, data=request.POST)
            if form.is_valid():
                form.save()
                ctx['saved'] = 1
                form = UserAuthorForm(instance=author)
            else:
                ctx['form'] = form
                ctx['saved'] = 2

        ctx['form'] = form
        return render(request, 'users/cabinet/main.html', ctx)
    else:
        return redirect('users:login')


def all_books(request):
    ctx = {}
    ctx['cabinet_tab'] = 'all_books'
    current_author = UserAuthor.objects.get(user_id=request.user.id)
    all_books = Book.objects.filter(author=current_author)
    ctx['all_books'] = all_books
    return render(request, 'users/cabinet/all_books.html', ctx)


def write_book(request):
    ctx = {}
    ctx['cabinet_tab'] = 'create_book'
    ctx['form'] = BookForm()
    if request.method == 'POST':
        form = BookForm(data=request.POST)
        current_author = UserAuthor.objects.get(user_id=request.user.id)
        if form.is_valid():
            form.save(author=current_author)
    return render(request, 'users/cabinet/write_book.html', ctx)


def books(request):
    ctx = {}
    ctx['all_books'] = Book.objects.all().order_by('-id')
    return render(request, 'users/books.html', ctx)


def search(request):
    ctx = {}
    user_email = request.GET.get('email')
    ctx['email'] = user_email
    all_orders = Order.objects.filter(email=user_email)
    ctx['all_orders'] = all_orders
    return render(request, 'users/search.html', ctx)


def my_order(request):
    ctx = {}
    all_orders = Order.objects.all()
    current_author = UserAuthor.objects.get(user_id=request.user.id)
    list_orders = []
    for order in all_orders:
        all_books = order.books.all()
        if all_books:
            all_books_by_current_author = [book for book in all_books if book.author == current_author]
            if all_books_by_current_author:
                order_obj = {
                    'order': order,
                    'books': all_books_by_current_author
                }
                list_orders.append(order_obj)
    total_sum_books = sum([book.price
                           for order in list_orders
                                for book in order['books']
                           ])
    commision = total_sum_books * 0.05  # percent from platform
    clean_price = total_sum_books - commision
    ctx['cabinet_tab'] = 'my_order'
    ctx['list_orders'] = list_orders
    ctx['commision'] = commision
    ctx['clean_price'] = clean_price
    return render(request, 'users/cabinet/my_order.html', ctx)
