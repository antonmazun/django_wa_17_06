from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class UserAuthor(models.Model):
    TYPE_USER_VIEW = (
        ('fio', 'ФИО'),
        ('pseudo_name', 'Псевдоним'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=1500, verbose_name='Биография')
    type_view = models.CharField(choices=TYPE_USER_VIEW, max_length=50, verbose_name='Тип представления0')
    pseudoname = models.CharField(max_length=150, verbose_name='Псевдоним', blank=True, null=True)

    def __str__(self):
        return str(self.user)

    def get_name(self):
        if self:
            if self.type_view == 'fio':
                return '{} {}'.format(self.user.first_name, self.user.last_name)
            elif self.type_view == 'pseudo_name':
                return self.pseudoname or 'anonim'


class Book(models.Model):
    TYPE_GENRE = (
        ('comedy', 'Комедия'),
        ('drama', 'Драма'),
        ('fantastic', 'Фантастик'),
    )
    title = models.CharField(max_length=500, verbose_name='Название')
    lead = models.TextField(max_length=1500, verbose_name='Краткое описание книги')
    genre = models.CharField(choices=TYPE_GENRE, max_length=50, verbose_name='Жанр')
    rate = models.IntegerField(default=0, verbose_name='Рейтинг')
    author = models.ForeignKey(UserAuthor, on_delete=models.SET_NULL, blank=True, null=True)
    price = models.IntegerField(default=100, verbose_name='Цена')

    def __str__(self):
        return self.title


class Order(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Имя'
                                  , blank=True, null=True, default='')
    last_name = models.CharField(max_length=255, verbose_name='Фамилия'
                                 , blank=True, null=True, default='')
    middle_name = models.CharField(max_length=255, verbose_name='Отчество'
                                   , blank=True, null=True, default='')
    email = models.EmailField(max_length=255, verbose_name='E-mail')
    phone_number = models.CharField(max_length=13, verbose_name='Номер телефона')
    books = models.ManyToManyField(Book)
    total_price = models.FloatField(blank=True, null=True, default=0)
    msg = models.TextField(max_length=6000, verbose_name='Комментарий к заказу', blank=True, null=True, default='')

    def __str__(self):
        return 'Заказ № {}. От {} {} {}'.format(self.id, self.first_name, self.last_name, self.phone_number)
