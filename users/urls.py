from django.urls import path
from . import views
from . import auth_views
from . import session_views

app_name = "users"

urlpatterns = [
    path('cabinet-route-asdasd/', views.cabinet, name='cabinet'),
    path('all-books-user/', views.all_books, name='all_books'),
    path('write_book/', views.write_book, name='write_book'),
    path('books/', views.books, name='books'),
    path('search/', views.search, name='search'),
    path('my_order/', views.my_order, name='my_order'),
]

x = [
    path('register', auth_views.register, name='register'),
    path('login', auth_views.login, name='login'),
    path('logout', auth_views.logout, name='logout'),
]

session_urls = [
    path('add_to_session/<int:book_id>/', session_views.add_to_session, name='add_to_session'),
    path('bucket', session_views.bucket, name='bucket'),
    path('delete_from_bucket/<int:pk>/', session_views.delete_from_bucket, name='delete_from_bucket'),
    path('approve_order', session_views.approve_order, name='approve_order'),
]

urlpatterns += x
urlpatterns += session_urls
