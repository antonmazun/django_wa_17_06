from django.db import models


# Create your models here.


class Author(models.Model):
    TYPE_GENRE = (
        ('comedy', 'Комедия'),
        ('drama', 'Драма'),
        ('fantastic', 'Фантастик'),
    )

    name = models.CharField(max_length=255, verbose_name='Имя автора')
    surname = models.CharField(max_length=255, verbose_name='Фамилия автора')
    genre = models.CharField(choices=TYPE_GENRE , max_length = len('fantastic'))

    def __str__(self):
        return '{} {}: {}'.format(
            self.name, self.surname, self.genre
        )

    def get_full_name(self):
        return '{} {}'.format(self.name.capitalize() , self.surname.capitalize())

    def get_books(self):
        return Book.objects.filter(author=self)

    def get_total_price(self):
        all_books  = self.get_books()
        total_price = 0
        sale_price  = 0
        for book in all_books:
            if book.is_sale:
                total_price += book.new_price
                sale_price += book.price  - book.new_price
            else:
                total_price += book.price
        return {
            'total_price': total_price,
            'sale_price': sale_price,
        }


class Book(models.Model):
    author  = models.ForeignKey(Author,on_delete=models.CASCADE)
    title = models.CharField(max_length=500, verbose_name="Заголовок книги")
    price = models.IntegerField()
    is_sale = models.BooleanField(default=False)
    new_price = models.IntegerField(blank=True, null=True)
    desc = models.TextField(verbose_name="Описание книги" , max_length=1500)

    def __str__(self):
        return '{} {}'.format(self.title ,self.price)

    def calc_economy(self):
        if self.is_sale:
            return {
                'money': self.price - self.new_price,
                'percent': round((1 - self.new_price / self.price) * 100 , 2)
            }
