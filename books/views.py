from django.shortcuts import render
from .models import Book , Author
# Create your views here.


def books_home(request):
    ctx  = {}
    ctx['active_tab'] = 'books'
    ctx['all_books'] = Book.objects.all()
    return render(request , 'books/home.html' , ctx)


def book_detail(request , pk):
    ctx  = {}
    ctx['active_tab'] = 'books'
    ctx['book'] = Book.objects.get(id=pk)
    return render(request , 'books/book_detail.html' , ctx)


def author_detail(request , pk):
    ctx = {}
    ctx['author'] = Author.objects.get(id=pk)
    return render(request , 'books/author_detail.html' , ctx)