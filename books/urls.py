from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.books_home, name='main'),
    path('book-detail/<int:pk>/', views.book_detail , name='book_detail'),
    path('author/<int:pk>/', views.author_detail),
]
