"""eshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home.models import Article
import random
from django.http import HttpResponse
from django.shortcuts import render, redirect

from django.template import RequestContext

from django.conf.urls import (
    handler400, handler403, handler404, handler500
)



handler404 = 'home.views.page_not_found'



RANDOM_TEXT = """
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias, quibusdam? Adipisci aut consequuntur
    dolore doloremque dolorum enim eum fugiat magni maxime minus neque non officiis optio perspiciatis quam qui ratione
    saepe sed sequi sit, tempore vitae voluptas. Animi architecto, dolorem eos excepturi fugit maxime necessitatibus
    quaerat qui! Ad animi aut cum delectus deserunt dolor earum eius eligendi error est, et excepturi illo impedit
    ipsum, minus nesciunt quia quisquam reiciendis! Consequuntur maiores necessitatibus tempore? A alias autem
    consequuntur corporis cum cumque cupiditate dolorum eveniet harum inventore, iure modi omnis perspiciatis, quas
    quasi qui quibusdam rem sapiente sed similique sunt veritatis.
""".split()


def generate_articles(request):
    for i in range(1000):
        new_article = Article()
        new_article.title = ' '.join(random.sample(RANDOM_TEXT, 3))
        new_article.text = ' '.join(random.sample(RANDOM_TEXT, 74))
        new_article.price = random.randrange(578)
        new_article.save()
    return HttpResponse('Articles done!')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('generate_articles', generate_articles),
    path('books/', include('books.urls', namespace='books')),
    path('users/', include('users.urls', namespace='users'))
]
